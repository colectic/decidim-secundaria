# Mòdul 4: Taller de co-creació de processos

Durant les següents sessions dissenyarem, en grups i col·laborativament, els processos o espais de participació que escullin les alumnes. Treballarem de manera molt pràctica conceptes abordats en les sessions anteriors i dissenyarem com implementar el procès amb èxit, pensant en accions de dinamització dins i fora del Decidim.

- [Objectius del mòdul](#objectius-del-modul)
- [Requeriments](#requeriments)
- [Resultats d'aprenentge](#resultats)
- [Sessions](#sessions)
	- [Què puc decidir al meu institut?](#sessió-1)
	- [Implementar el meu procés](#sessió-2)
	- [Disseny del pla de comunicació](#sessió-3)
	- [Escollim els nostres processos](#sessió-4)



## Objectius del mòdul

* Crear processos participatius per presentar-los al Consell Escolar
* Plantejar els processos amb una mirada global
* Calendaritzar les accions que caldrà dur a terme

## Requeriments

Es demana que el grup hagi realitzat primer els mòduls d'administració. 
També es pot proposar realitzar les sessions d'administració de manera intercalada a les sessions, treballant de manera intercalada els elements a configurar. 

Material de reforç:

- [Creació d'un procés](https://www.youtube.com/watch?v=sBvfKfDYiIY)
- [Crear i gestionar enquestes](https://www.youtube.com/watch?v=tw-bwW0kL_o)
- [Convocar i gestionar trobades](https://www.youtube.com/watch?v=YhaDETwAjfQ&t=2s)
- [Recollida de propostes](https://www.youtube.com/watch?v=8odsgoLwhJ0&t=1s)
- [Els suports a les propostes](https://www.youtube.com/watch?v=y55mz1Imrpk)


## Resultats
Al finalizar aquest mòdul, les participants podran:

* accedir a la plataforma com a administradora
* crear un procés i configurar-ne el comportament segons cada fase
* preveure accions de suport per a la dinamització del procés.

## Sessions

-----

### Sessió 1
###Què puc decidir al meu institut?

La sessió inicial del taller de co-creació de processos està pensada per consensuar quins són els processos que es desenvoluparan al llarg de tot el taller.

#### Necessitaràs...
- Un espai àmpli on realitzar el debat en grup
- una pissarra on prendre notes de la sessió
- Checklist de creació de processos (link)

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats | 
| --------- | ----- | -----------| -------------------- | ------------------ | 
| Debat | 15 min | A partir de la pregunta: *què puc canviar del meu institut* es proposa extreure els temes principals que preocupen a les participants sobre el dia a dia del seu institut. <br> S'ha de dibuixar conjuntament quins són els espais de participació i de decisió que actualment existeixen en el seu institut. <br>Cal que quedi clar quins temes estan dins de la seva capacitat de decidir i quins no, a fi de no generar falses expectatives i frustració. <br> Alguns grups poden escollir dinamitzar alguns dels òrgans de participació detectats en l'organigrama.| Pissarra | Per parelles o grups de tres, escullen un tema per posar a debat amb tota la comunitat del centre. | 
| Treball en grup | 30 min | Es busca definr cada procés a partir de les preguntes:<br> - com es diu el procés?<br> - què es vol decidir? (descripció breu)<br> - com es vol decidir (descripció llarga i fases) <br>- quines accions realitzaràs (selecció dels components) | Material de suport: checklist per crear processos | Propostes definides i preparades per pujar-les al decidim | 
| Tancament | 10 | Avaluar l'estat dels processos plantejats, preveure si es poden començar a implementar al decidim a la següent sessió | | Creació d'una llista amb els processos plantejats | |

-----

### Sessió 2
### Implementar el meu procés

Les sessions d'implementació busquen ser un espai de creació col·lectiva, on es doni un procés d'aprenentatge entre iguals.

#### Necessitaràs...
- Aula àmplia amb taules per treballar en grup
- Un portàtil o ordinador de sobretaula per grup
- Ordinador i  projector per al professorat

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats | 
| --------- | ----- | -----------| -------------------- | ------------------ | 
| Introducció | 10 min | Ronda ràpida per saber quins processos s'estan treballant |  | | 
| Treball | 40 mim | Per grups de treball es creen i configuren els processos en el Decidim. Cada cop que es plantegin dubtes de com crear un component o configurar una acció, es demana al grup si algú altre de la classe els pot ajudar, i si no es sap com realitzar l'acció, s'explica a la pissarra i a tot el grup. <br> Es vol fomentar que es produeixi un aprenentatge entre iguals. | Un ordinador per cada grup d'estudiants | Processos enllestits per fer-se públics |\
| Tancament | 5 min | Ronda ràpida per veure en quin punt està cada procés | | | |

__\* Per desenvolupar la implementació del procés pot ser necessari dues o tres sessions de 55 min., segons el ritme assolit per cada grup.__

__Recomanem realitzar primer la planificació global seguint el calendari que s'han marcat a partir de la configuració de Fases, i posteriorment definir les accions concretes a partir dels components vinculats a cada fase.__

-----

### Sessió 3
### Disseny del pla de comunicació

Preveure com s'explicarà el procés a la resta de comunitat educativa.

#### Necessitaràs...

- Una graella per dissenyar el pla de comunicació per grup  [(link)](/docs/pla_comunica_Decidim.pdf)
- Revisar els comentaris per a facilitadores de la Graella del pla de comunicació [(link)](/docs/pla_comunica_Decidim_comentat.pdf)
- Taules per facilitar el treball en grup


| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats | 
| --------- | ----- | -----------| -------------------- | ------------------ | 
| Introducció | 10 min | Un cop dissenyat el nostre procés, necessitem planificar com el volem donar a conèixer. <br> És important planificar totes les accions on hi vulguem implicar a l'alumnat| | | 
| Treball | 35 min | Planificar totes les accions de comunicació segons: <br>- què vull comunicar? <br> - a qui vull comunicar-ho? <br> - Com ho faré? <br> Tenint en compte els canals i el llenguatge que faré servir en cada un dels casos.| Graella pel disseny del pla de comunicació [(link)](/docs/pla_comunica_Decidim.pdf)| Planificació de les accions comunicatives | 
| Tancament | 10 min | Ronda ràpida per saber què han fet els diferents grups. | | | |

### Sessió 4
### Escollim els nostres processos

Dinàmica de tancament del bloc de co-creació, en la que el mateix grup decideix quins dels processos presentar davant del Consell Escolar per poder desenvolupar desprès a tot el centre.

#### Necessitaràs...

- Espai ampli per debatre en grup
- Ordinador i projector
- Crear un procés per classe on es puguin realitzar la recollida de suports als projectes presentats.


| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats | 
| --------- | ----- | -----------| -------------------- | ------------------ | 
| Introducció | 10 min | L'objectiu de la última sessió del bloc és consensuar entre tota la classe quins són els processos que es presentaran al Consell Escolar | Prèviament hem publicat un procés privat per la classe on hem penjat les diferents propostes de processos | | 
| Treball | 35 min | Presentació ràpida de cada un dels processos davant de tot el grup| | | 
| Tancament | 10 min | Priorització de les propostes fent servir la recollida de suports en el procés creat per la classe. | | Priorització realizada entre tot el grup | |
