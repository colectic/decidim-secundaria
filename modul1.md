# Mòdul 1: Democràcia i drets digitals

Sessions inicials amb les que es busca donar coneixiements pràctics sobre què implica el treball en xarxa en un context de capitalisme extractiu de dades. Es planegen tres sessions, podent ampliar alguns continguts amb material complementari si es considera oportú.

3h

- [Objectius del mòdul](#objectius-del-modul)
- [Requeriments](#requeriments)
- [Resultats d'aprenentge](#resultats)
- [Sessions](#sessions)
	- [Introducció. capitalisme digital i dades](#sessio-1)
	- [Aprofundiment. Què passa quan ens connectem a Internet?](#sessio-2)
	- [Reflexió. Els rastres digitals](#sessio-3)

___

## Objectius del mòdul
- Conèixer quins són els efectes de la nostra vida digital: què són els rastres digitals, quina conseqüència tenen per la nostra intimitat i la nostra privacitat.
- Entendre Internet com un lloc on desenvolupar-se com a persona.
- Dotar eines per fer-ho de manera segura.


## Requeriments

Per desenvupar aquestes sessions de sensibilització necessitarem fomentar una actitud deliberativa entre els i les joves. Poder realitzar aquestes sessions en un espai ample amb el recolzament d'un projector pot ajudar (sala d'actes o similar) sobretot si es permet re-estructurar les cadires en forma de cercle o semi-cercle.

Material de reforç:

- Sobre els rastres digitals [My and my shadow](https://myshadow.org/es)
- [Data detox kit](https://datadetoxkit.org/ee/families/datadetox-x-youth/) sobre privacitat digital, seguretat digital, benestar digital i desinformació.




## Resultats
Al finalitzar aquest mòdul, les participants podran:
- Els alumnes són concients dels riscos d'interactuar amb plataformes digitals gestionades per les GAFAM
- Els alumnes s'interessen per altres maneres de participar en línia de manera segura i crítica


## Sessions


------

### Sessió 1
#### Introducció. Capitalisme digital i dades

En aquesta sessió introduïrem els conceptes clau sobre privacitat en l'esfera d'internet, situant-lo en un marc de capitalisme de dades.

#### Necessitaràs...

- ordinador i projector
- presentació (link)
- Apunts sobre la presentació (link)

#### Detall de la sessió

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 25 min  | Presentació a través de la qual introduïm diversos temes: <br> - Context on hi ha un augment dels espais digitals<br> - Entendre la tecnologia sobre les comunicacions com un valor estratègic <br>- també com un valor comercial <br> - quin paper tenen els estats en el control de les comunicacions?<br> - concpte de ciutadania digital i models democràtics| Presentació (link) | Despertar curiositat i dubtes sobre l'activitat digital dels i les pròpies joves. |
| Debat | 20 min | Posar en qüestió moltes de les activitats digitals dels joves, així com les eines i serveis que fem servir. | |  |
| Tancament | 10 min | Explicació dels objectius el curs: realitzar processos de participació fent servir la plataforma Decidim del propi centre. |  |  |  |


----


### Sessió 2
### Aprofundiment. Què passa quan ens connectem a Internet?

Amb aquesta sessió busquem treballar el coneixement entorn com funcionen les connexions a Internet, mostrant que és una estructura material que comporta conseqüències reals amb les persones que el fan servir.

Entrem a analitzar amb detall què passa quan una persona es connecta a Internet.
Sessió dinàmica per realitzar en un espai ampli o a l'aire lliure.

#### Necessitaràs...
- Imprimir les targetes *Com funciona Internet?*
- Una aula o espai ampli on poder situar-nos en cercle


#### Detall de la sessió

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | Es preparen tantes targetes com persones realtizaran la dinàmica. Cada targeta és un element de la xarxa d'Internet. <br>Cada persona ha de saber què fa exactament el seu element. | Model de targetes |  |
| Privacitat | 15 min | Seguint el model de la EFF, expliquem què passa quan ens connectem a aqualsevol pàgina que requereixi autentificació http/https| [Esquema de les connexions](https://www.eff.org/pages/tor-and-https)|  |
| Anonimat | 15 min |Seguint el mateix model, expliquem opcions per anonimitzar la nostra activitat digital |[Esquema de les connexions](https://www.eff.org/pages/tor-and-https) | |
| Debat | 15 min | Et sens segura quan naveges per la xarxa? què et fa sentir segur i què et fa sentir insegur? |  | Introduir conceptes de privacitat i anonimat |

---

### Sessió 3
### Reflexió. Els rastres digitals

Arran de la sessió anterior, es pot fer evident la necessitat de conèixer realment quines dades estem deixant cada vegada que ens connectem.

#### Necessitaràs...

- ordinador i projector
- instal·lar la extensió lightbeam (opcional)
- sentir-te còmode explorant [Trackography](https://trackography.org/#)
- imprimir el material [Data detox kit](https://cdn.ttc.io/s/datadetoxkit.org/youth/Data-Detox-x-Youth_ES-ES.pdf) - Recomanem realitzar les pàgines pàgines 2 i 3, per treballar la privacitat digital


#### Detall de la sessió

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | Què son els rastres digitals? Explicar que la nostra vida digital es divideix en els continguts que generem i en els rastres que deixem, i cap dels dos conceptes són senzills d'eliminar.<br>  Visualitzar els rastres digitals que deixem a través de la navegació amb lightbeam o amb Trackography. |  |  |
| Treball | 35 min |Repartir un Data detox kit per a cada estudiant. Ens centrarem a treballar la privacitat digital, per tant només repartirem les corresponents a aquest tema (2 i 3) | Data detox kit |  |
| Tancament | 25 min | Debat amb les principals conclusions de l'activitat realitzada |   | Els alumnes són conscients de les exposions que comporta fer servir les seves xarxes socials |

\* Es pot treballar el *Data detox kit* complet ampliant les sessions destinades a aquesta temàtica.


