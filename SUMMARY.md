# Summary

* [Introducció](README.md)
* [Mòdul 1: Democràcia i drets digitals](modul1.md)
- [Mòdul 2: Què és Decidim?](modul2.md)
- [Mòdul 3: Administració del Decidim](modul3.md)
- [Mòdul 4: Taller de co-creació de processos](modul4.md)
- [Mòdul 5: Tancament de processos i resultats](modul5.md)
----
- [Formació a professorat](professorat.md)

