# Mòdul 5: Acompanyament al procés

XXh

## Objectius del mòdul
- Implementar un o dos processos participatius a tot el Centre
- Fomentar la participació entre l'alumnat
- Garantir un retorn i devolució del procés


## Requeriments

### Coneixements prèvis
- Haver realitzat el mòdul de co-creació de processos

### Aptituds
- 

## Resultat d'aprenentatge
- Conèixer la comunitat educativa i esdevenir-ne membre actiu
- Localitzar

## Sessions

### 1. Fomentar la participació

Havent escollit

| Activitat | Temps | Descripció | Materials i recursos | Resultats esperats |
| --------- | ----- | -----------| -------------------- | ------------------ |
| Introducció | 10 min  | |  |  |
| Treball | 35 min | | |  |
| Tancament | 10 min |  |  |  |  |


### 2. La comunicació presencial


### 3. El retorn d'un procés participatiu
