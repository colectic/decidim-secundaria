# Decidim a centres educatius

## Currículum formatiu adreçat a 3r i 4t d’ESO, i a Grau Mitjà en Informàtica i Comunicacions de Cicles Formatius.

Aquest currículum està pensat per oferir una visió general sobre drets i deures de la ciutadania digital, aprofundint en les potencialitats de la participació i mecanismes per a realitzar-la de manera segura. Aborda el disseny de processos participatius i es treballa amb profunditat com gestionar-los fent servir la instància de Decidim d'un Centre educatiu.

L’itinerari curricular finalitza treballant estratègies de comunicació en línia (redacció, publicació de continguts audiovisuals) i comunicació presencial (estratègies per a parlar en públic per realitzar una bona presentació. 

---


El currículum es composa de 5 mòduls

- [Mòdul 1. Presentació. Conceptes generals sobre democràcia, participació, software lliure i drets digitals](modul1.md)
- [Mòdul 2: Introducció bàsica a la plataforma Decidim, registre, espais i components](modul2.md)
- [Mòdul 3: Panell d’administració de la plataforma i utilització dels diferents components](modul3.md)
- [Mòdul 4: Taller de co-creació de processos](modul4.md)
- [Mòdul 5: Tancament de processos i resultats](modul5.md)

S'acompanyarà aquest itinerari formatiu amb formació específica al professorat per a dotar-los d'eines i recursos per dinamitzar els processos amb independència del programa pilot.

- [Formació a professorat](professorat.md)
